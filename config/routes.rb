Rails.application.routes.draw do
  resources :applications, only: [:index, :show, :create, :destroy]
  put '/applications/:id/mark_as_seen', to: 'applications#mark_as_seen', as: 'mark_as_seen'

  resources :postings, only: [:index, :show, :create, :update, :destroy]

  resource :session, only: [:create, :destroy]

  resources :users, only: [:create]
end
