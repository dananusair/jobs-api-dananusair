require 'rails_helper'

RSpec.describe Posting, type: :model do

  fixtures :users

  describe 'validations' do
    it 'should be valid with valid attributes' do
      user = users(:user1)
      posting = Posting.new(user: user, title: 'title', description: 'description')
      expect(posting).to be_valid
    end

    it 'should be invalid with title equals nil' do
      user = users(:user1)
      posting = Posting.new(user: user, title: nil, description: 'description')
      expect(posting).to_not be_valid
    end

    it 'should be invalid with description equals nil' do
      user = users(:user1)
      posting = Posting.new(user: user, title: 'title', description: nil)
      expect(posting).to_not be_valid
    end

    it 'should be invalid if title was empty string' do
      user = users(:user1)
      posting = Posting.new(user: user, title: '', description: 'description')
      expect(posting).to_not be_valid
    end

    it 'should be invalid if description was empty string' do
      user = users(:user1)
      posting = Posting.new(user: user, title: 'title', description: '')
      expect(posting).to_not be_valid
    end

    it 'should be invalid with title & description equal nil' do
      user = users(:user1)
      posting = Posting.new(user: user, title: nil, description: nil)
      expect(posting).to_not be_valid
    end

    it 'should be invalid if title & description was empty string' do
      user = users(:user1)
      posting = Posting.new(user: user, title: '', description: '')
      expect(posting).to_not be_valid
    end

    it 'should not create a job posting if user is not admin' do
      user = users(:user2)
      posting = Posting.new(user: user, title: 'title', description: 'description')
      expect(posting).to_not be_valid
    end
  end

  describe 'associations' do
    it 'belongs to user' do
      assc = described_class.reflect_on_association :user
      expect(assc.macro).to eq :belongs_to
    end

    it 'has many applications' do
      assc = described_class.reflect_on_association :applications
      expect(assc.macro).to eq :has_many
    end
  end
end
