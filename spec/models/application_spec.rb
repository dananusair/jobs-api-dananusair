require 'rails_helper'

RSpec.describe Application, type: :model do

  fixtures :users
  fixtures :postings

  describe 'validations' do
    it 'should be valid with valid attributes' do
      user = users(:user2)
      posting = postings(:posting1)
      posting = Application.new(user: user, posting: posting)
      expect(posting).to be_valid
    end

    it 'should be invalid if user is admin' do
      user = users(:user1)
      posting = postings(:posting1)
      application = Application.new(user: user, posting: posting)
      expect(application).to_not be_valid
    end

    it 'should not create application if user has already applied for posting' do
      user = users(:user1)
      posting = postings(:posting1)
      application1 = Application.create(user: user, posting: posting)
      application2 = Application.new(user: user, posting: posting)
      expect(application2).to_not be_valid
    end
  end

  describe 'associations' do
    it 'belongs to user' do
      assc = described_class.reflect_on_association :user
      expect(assc.macro).to eq :belongs_to
    end

    it 'has many applications' do
      assc = described_class.reflect_on_association :posting
      expect(assc.macro).to eq :belongs_to
    end
  end

  describe '#before_create' do
    it 'sets status as \'Not Seen\' before creation' do
      user = users(:user2)
      posting = postings(:posting1)
      application = Application.create(user: user, posting: posting)
      expect(application.status).to eq('Not Seen')
    end
  end
end
