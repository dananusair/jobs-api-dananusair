require 'rails_helper'

RSpec.describe User, type: :model do

  fixtures :users

  describe 'validations' do
    it 'should be valid with valid attributes' do
      user = User.new(email: 'test_user@mail.com', password: 'password', password_confirmation: 'password', admin: false)
      expect(user).to be_valid
    end

    it 'should be invalid if email already exists' do
      user = User.new(email: 'test@mail.com',  password: 'password', password_confirmation: 'password', admin: false)
      expect(user).to_not be_valid
    end

    it 'should be invalid without a password' do
      user = User.new(email: 'test_user@mail.com', password: nil, password_confirmation: nil)
      expect(user).to_not be_valid
    end

    it 'should be invalid with a short password' do
      user = User.new(email: 'test_user@mail.com', password: 'short', password_confirmation: 'short')
      expect(user).to_not be_valid
    end

    it 'should be invalid with wrong password_confirmation' do
      user = User.new(email: 'test_user@mail.com', password: 'short', password_confirmation: 'long')
      expect(user).to_not be_valid
    end

    it 'should not be able to create another admin' do
      user = User.new(email: 'other_admin@mail.com', password: 'password', password_confirmation: 'password', admin: true)
      expect(user).to_not be_valid
    end
  end

  describe 'associations' do
    it 'has many postings' do
      assc = described_class.reflect_on_association :postings
      expect(assc.macro).to eq :has_many
    end

    it 'has many applications' do
      assc = described_class.reflect_on_association :applications
      expect(assc.macro).to eq :has_many
    end
  end

  describe '#after_initialization' do
    it 'admin value should be false by default' do
      user = User.new(email: 'test_user@mail.com', password: 'short', password_confirmation: 'long')
      expect(user.admin).to be_falsey
    end
  end
end
