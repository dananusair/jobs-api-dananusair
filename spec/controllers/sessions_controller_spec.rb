require 'rails_helper'
require 'request_helper'
include RequestHelper

RSpec.describe SessionsController, type: :controller do
  subject {
    User.new(
      id: 1,
      username: 'new',
      email: 'new@mail.com',
      password: 'password'
    )
  }

  describe 'POST #create' do
    before(:each) do
      subject.save
    end

    context 'with valid parameters' do
      let(:valid_params) do
        {
          session: {
            email: subject.email,
            password: subject.password
          }
        }
      end

      it 'creates a new user session' do
        post :create, params: valid_params
        expect(response).to have_http_status(:created)
      end

      it 'does not create session if already signed in' do
        sign_in(subject)
        post :create, params: valid_params
        expect(session[:user_id]).to_not eq(subject.id)
      end
    end

    context 'with invalid parameters' do
      let(:invalid_params) do
        {
          session: {
            email: subject.email,
            password: 'wrongpassword'
          }
        }
      end

      it 'does not create a new user session' do
        post :create, params: invalid_params
        expect(response).to_not be_successful
      end
    end
  end

  describe 'DELETE #destroy' do
    it 'destroys user session' do
      subject.save
      sign_in(subject)
      delete :destroy
      expect(session[:user_id]).to be_nil
    end
  end
end
