require 'rails_helper'
require 'request_helper'
include RequestHelper

RSpec.describe ApplicationsController, type: :controller do
  fixtures :users
  fixtures :postings

  subject {
    Application.new(
      id: 1,
      user: users(:user2),
      posting: postings(:posting1)
    )
  }

  describe 'GET index' do
    it 'retrieves all applications' do
      sign_in(users(:user1))
      get :index
      expect(response).to have_http_status(:ok)
    end
  end

  describe 'GET show' do
    it 'retrieves a specific application' do
      sign_in(users(:user1))
      subject.save
      get :show, params: { id: subject.id }
      expect(response).to have_http_status(:ok)
    end
  end

  describe 'POST #create' do
    before(:each) do
      sign_in(users(:user2))
    end

    context 'with valid parameters' do
      it 'creates a new application' do
        expect {
          post :create, params: { posting_id: postings(:posting1).id }
        }.to change(Application, :count).by(1)
      end

      it 'responds to status :created' do
        post :create, params: { posting_id: postings(:posting1).id }
        expect(response).to have_http_status(:created)
      end
    end

    context 'with invalid parameters' do
      it 'does not create a new application' do
        expect {
          post :create, params: { posting_id: nil }
        }.to_not change(Application, :count)
      end

      it 'expects creation to be unsuccessful' do
        post :create, params: { posting_id: nil }
        expect(response).to_not be_successful
      end
    end
  end

  describe 'DELETE #destroy' do
    before(:each) do
      sign_in(users(:user2))
      subject.save
    end

    it 'destroys a application' do
      expect {
        delete :destroy, params: { id: subject.id }
      }.to change(Application, :count).by(-1)
    end

    it 'responds with status :no_content' do
      delete :destroy, params: { id: subject.id }
      expect(response).to have_http_status(:no_content)
    end
  end

  describe 'PUT #mark_as_seen' do
    context 'when signed_in user is admin' do
      before(:each) do
        sign_in(users(:user1))
        subject.save
      end

      it 'allows admin to mark application as seen' do
        put :mark_as_seen, params: { id: subject.id }
        expect(subject.status).to be_truthy
      end

      it 'responds with status :ok when successful' do
        put :mark_as_seen, params: { id: subject.id }
        expect(response).to have_http_status(:ok)
      end
    end

    context 'when sign_in user is not admin' do
      before(:each) do
        sign_in(users(:user2))
        subject.save
      end

      it 'does not allow user to mark application as seen' do
        expect {
          put :mark_as_seen, params: { id: subject.id }
        }.to_not change(subject, :status)
      end

      it 'responds with status :forbidden' do
        put :mark_as_seen, params: { id: subject.id }
        expect(response).to have_http_status(:forbidden)
      end
    end
  end
end
