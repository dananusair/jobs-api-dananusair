require 'rails_helper'
require 'request_helper'
include RequestHelper

RSpec.describe PostingsController, type: :controller do
  fixtures :users

  subject {
    Posting.new(
      id: 1,
      title: 'title',
      description: 'description',
      user: users(:user1)
    )
  }

  describe 'GET index' do
    it 'retrieves all postings' do
      sign_in(users(:user1))
      get :index
      expect(response).to have_http_status(:ok)
    end
  end

  describe 'GET show' do
    it 'retrieves a specific posting' do
      sign_in(users(:user1))
      subject.save
      get :show, params: { id: subject.id }
      expect(response).to have_http_status(:ok)
    end
  end

  describe 'POST #create' do
    before(:each) do
      sign_in(users(:user1))
    end

    context 'with valid parameters' do
      let(:valid_params) do
        {
          posting: {
            title: subject.title,
            description: subject.title
          }
        }
      end

      it 'creates a new posting' do
        expect {
          post :create, params: valid_params
        }.to change(Posting, :count).by(1)
      end

      it 'responds to status :created' do
        post :create, params: valid_params
        expect(response).to have_http_status(:created)
      end
    end

    context 'with invalid parameters' do
      let(:invalid_params) do
        {
          posting: {
            title: nil,
            description: nil
          }
        }
      end

      it 'does not create a new posting' do
        expect {
          post :create, params: invalid_params
        }.to_not change(Posting, :count)
      end

      it 'expects creation to be unsuccessful' do
        post :create, params: invalid_params
        expect(response).to_not be_successful
      end
    end
  end

  describe 'PUT #update' do
    before(:each) do
      sign_in(users(:user1))
      subject.save
    end

    context 'with valid parameters' do
      it 'successfully updates a posting' do
        put :update, params: { id: subject.id, posting: { title: 'new title' } }
        expect(response).to be_successful
      end
    end

    context 'with invalid parameters' do
      it 'updates a posting' do
        put :update, params: { id: subject.id, posting: { title: nil } }
        expect(response).to_not be_successful
      end
    end
  end

  describe 'DELETE #destroy' do
    before(:each) do
      sign_in(users(:user1))
      subject.save
    end

    it 'destroys a posting' do
      expect {
        delete :destroy, params: { id: subject.id }
      }.to change(Posting, :count).by(-1)
    end

    it 'responds with status :no_content' do
      delete :destroy, params: { id: subject.id }
      expect(response).to have_http_status(:no_content)
    end
  end
end
