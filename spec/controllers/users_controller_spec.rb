require 'rails_helper'

RSpec.describe UsersController, type: :controller do
  fixtures :users

  describe 'POST #create' do
    context 'with valid parameters' do
      let(:valid_params) do
        {
          user: {
            email: 'new@mail.com',
            password: 'password',
            password_confirmation: 'password',
          }
        }
      end

      it 'creates a new user' do
        expect {
          post :create, params: valid_params
        }.to change(User, :count).by(1)
      end

      it 'responds with status :created' do
        post :create, params: valid_params
        expect(response).to have_http_status(:created)
      end
    end

    context 'with invalid parameters' do
      let(:invalid_params) do
        {
          user: {
            email: 'new@mail.com',
            password: 'short'
          }
        }
      end

      it 'does not create a new user' do
        expect {
          post :create, params: invalid_params
        }.to_not change(User, :count)
      end

      it 'expects creation to be unsuccessful' do
        post :create, params: invalid_params
        expect(response).to_not be_successful
      end
    end
  end
end
