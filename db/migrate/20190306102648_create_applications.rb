class CreateApplications < ActiveRecord::Migration[5.2]
  def change
    create_table :applications do |t|
      t.string :status
      t.integer :user_id
      t.integer :posting_id

      t.timestamps null: false
    end

    add_foreign_key :applications, :users
    add_foreign_key :applications, :postings
  end
end
