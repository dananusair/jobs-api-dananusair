class CreatePostings < ActiveRecord::Migration[5.2]
  def change
    create_table :postings do |t|
      t.string :title, null: false
      t.text :description, null: false
      t.integer :user_id

      t.timestamps null: false
    end

    add_foreign_key :postings, :users
  end
end
