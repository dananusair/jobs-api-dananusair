class PostingsController < ApplicationController
  before_action :set_posting, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource

  def index
    search = params[:search]
    @postings = if !search.blank?
      Posting.search(search, fields: [:title, :description])
    else
      @postings = Posting.all
    end

    render json: @postings
  end

  def show
    render json: @user
  end

  def create
    @posting = Posting.new(posting_params.merge({user: current_user}))

    if @posting.save
      render json: @posting, status: :created
    else
      render json: @posting.errors, status: :unprocessable_entity
    end
  end

  def update
    if @posting.update(posting_params)
      render json: @posting, status: :ok
    else
      render json: @posting.errors, status: :unprocessable_entity
    end
  end

  def destroy
    if @posting.destroy
      head :no_content
    else
      render json: @posting.errors, status: :unprocessable_entity
    end
  end

  private

  def set_posting
    @posting = Posting.find_by(id: params[:id])
  end

  def posting_params
    params.require(:posting).permit(:title, :description)
  end
end
