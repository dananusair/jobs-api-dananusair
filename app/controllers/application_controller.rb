class ApplicationController < ActionController::API
  before_action :authenticate

  rescue_from CanCan::AccessDenied do |exception|
    render json: { error: "this user is not authorized for this action" }, status: :forbidden
  end

  def sign_in(user)
    session[:user_id] = user.id
    @current_user = user
  end

  def sign_out
    session.delete(:user_id)
    @current_user = nil
  end

  def current_user
    if session[:user_id]
      @current_user ||= User.find_by(id: session[:user_id])
    end
  end

  def signed_in?
    !current_user.nil?
  end

  def authenticate
    head :unauthorized if !signed_in?
  end
end
