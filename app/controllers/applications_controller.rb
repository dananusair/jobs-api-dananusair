class ApplicationsController < ApplicationController
  before_action :set_application, only: [:show, :destroy, :mark_as_seen]
  load_and_authorize_resource

  def index
    if current_user.admin == false
      @applications = Application.where(user: current_user)
    else
      @applications = Application.all
    end

    render json: @applications
  end

  def show
    render json: @application
  end

  def create
    @application = Application.new(user: current_user, posting_id: params[:posting_id])

    if @application.save
      render json: @application, status: :created
    else
      render json: @application.errors, status: :unprocessable_entity
    end
  end

  def destroy
    if @application.destroy
      head :no_content
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  def mark_as_seen
    if signed_in? && current_user.admin? && @application.status == 'Not Seen'
      @application.assign_attributes(status: 'Seen')
      render json: @application.errors, status: :unprocessable_entity if !@application.save
    end

    render json: @application, status: :ok
  end

  private

  def set_application
    @application = Application.find(params[:id])
  end
end
