class UsersController < ApplicationController
  skip_before_action :authenticate, only: [:create]

  def create
    @user = User.new(user_parameters)

    if @user.save
      render json: @user, status: :created
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  private

  def user_parameters
    params.require(:user).permit(:username, :email, :password, :password_confirmation, :admin)
  end
end
