class SessionsController < ApplicationController
  skip_before_action :authenticate, only: [:create]

  def create
    return render json: { error: "already signed in" } if current_user

    @user = User.find_by(email: session_params[:email])

    if @user && @user.authenticate(session_params[:password])
      sign_in(@user)
      render json: @user, status: :created
    else
      render json: { error: "authentication failed" }, status: :unprocessable_entity
    end
  end

  def destroy
    sign_out
    head :no_content
  end

  private

  def session_params
    params.require(:session).permit(:email, :password)
  end
end
