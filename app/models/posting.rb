class Posting < ApplicationRecord
  searchkick text_middle: [:title, :description]

  validates_presence_of :title, :description

  belongs_to :user
  has_many :applications, dependent: :destroy

  validate do
    if !user.admin?
      errors.add(:user, "can not create a job posting")
    end
  end

  after_save do
    Posting.reindex
  end

  def search_data
    {
      title: title,
      description: description
    }
  end
end
