class User < ApplicationRecord
  include ApplicationHelper

  has_secure_password

  validates :password, length: { minimum: 8 }

  has_many :postings, dependent: :destroy
  has_many :applications, dependent: :destroy

  validate do
    if admin == true && User.exists?(admin: true)
      errors.add(:admin, "there can only be one admin!")
    end

    if User.exists?(email: email)
      errors.add(:email, "already in use")
    end
  end
end
