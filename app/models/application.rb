class Application < ApplicationRecord
  validate :validate_upon_creation, :on => :create
  validates_uniqueness_of :posting_id, scope: :user_id

  belongs_to :user, -> { where(admin: false) }
  belongs_to :posting

  before_create do
    self.status = 'Not Seen'
  end

  private

  def validate_upon_creation
    if user.admin?
      errors.add(:admin, "can not create job applications.")
    end
  end
end
